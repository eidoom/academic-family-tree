#!/usr/bin/env python3

import argparse
import re
import json

# import pickle

# from ruamel.yaml import YAML
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


def get_doctor(driver, url, doctors, t=False):
    driver.get(url)

    doctor = {
        "name": driver.find_element_by_tag_name("h2").text,
        "id": int(url.split("=")[-1]),
    }

    try:
        doctor["subject"] = driver.find_element_by_xpath(
            "//div[contains(text(),'Mathematics Subject Classification: ')]"
        ).text.split("—")[-1]
    except NoSuchElementException:
        pass

    dissertation = driver.find_elements_by_id("thesisTitle")
    dissertation = [d for d in dissertation if d != "(None)"]
    if any(d.text for d in dissertation):
        doctor["dissertation"] = []
        for d in dissertation:
            dd = d.text
            if dd and dd not in doctor["dissertation"]:
                doctor["dissertation"].append(dd)

    top = driver.find_element_by_xpath("//div[@id='paddingWrapper']")

    try:
        raw = top.find_elements_by_xpath("div/span/span/..")
        all_degrees = []
        all_years = []
        university = top.find_elements_by_xpath("div/span/span")
        if any(u.text for u in university):
            doctor["university"] = []
        for u, r in zip(university, raw):
            uu = u.text
            info = r.text
            if uu:
                if uu not in doctor["university"]:
                    doctor["university"].append(uu)
                degree = info.split(u.text)[0].strip()
            else:
                degree = re.match(r"[^0-9]*", info).group(0).strip()
            if degree and degree not in all_degrees:
                all_degrees.append(degree)
            years = re.findall(r"\d{4}", info)
            for year in years:
                if year:
                    y = int(year)
                    if y not in all_years:
                        all_years.append(y)
        if all_degrees:
            doctor["degree"] = all_degrees
        if all_years:
            doctor["year"] = all_years
    except NoSuchElementException:
        pass

    try:
        countries = top.find_elements_by_xpath("div/img")
        if countries:
            doctor["country"] = []
            for country in countries:
                c = country.get_attribute("alt")
                if c not in doctor["country"]:
                    doctor["country"].append(c)
    except NoSuchElementException:
        pass

    links = []
    try:
        advisor_links = driver.find_elements_by_xpath(
            "//p[contains(text(),'Advisor')]/a"
        )
        if advisor_links:
            doctor["advisors"] = []
            for advisor_link in advisor_links:
                advisor = advisor_link.text
                doctor["advisors"].append(advisor)
                if not any(existing["name"] == advisor for existing in doctors):
                    link = advisor_link.get_attribute("href")
                    links.append(link)
    except NoSuchElementException:
        pass

    doctors.append(doctor)

    if not t:
        for link in links:
            get_doctor(driver, link, doctors)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Scrape academic family tree traced back from scholar of id. Uses the Mathematics Genealogy Project as source <https://genealogy.math.ndsu.nodak.edu>."
    )
    parser.add_argument("id", help="The id of the scholar to start the search from")
    parser.add_argument(
        "-t",
        action="store_true",
        help="Test: print scraped info for scholar of given id only",
    )
    args = parser.parse_args()

    start = "https://genealogy.math.ndsu.nodak.edu/id.php?id=" + args.id
    driver = webdriver.Firefox()
    doctors = []
    get_doctor(driver, start, doctors, args.t)
    if args.t:
        print(doctors)
    else:
        # with open("data.pickle", "wb") as f:
        #     pickle.dump(doctors, f)

        # and open with:
        # with open("data.pickle", "rb") as f:
        #     d = pickle.load(f)

        # yaml = YAML()

        # with open("data.yaml", "w") as f:
        #     yaml.dump(doctors, f)

        with open("data.json", "w") as f:
            json.dump(doctors, f)

    driver.close()
