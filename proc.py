#!/usr/bin/env python3

import json


if __name__ == "__main__":
    with open("data.json", "r") as f:
        d = json.load(f)

    # i = 0
    nodes = []
    for scholar in d:
        # nodes.append({"id": i, "name": scholar["name"]})
        # i += 1
        nodes.append(
            {
                "id": scholar["name"],
                # "year": min(scholar["year"]),
            }
        )

    # trans = {a["name"]: a["id"] for a in nodes}

    edges = []
    for scholar in d:
        try:
            for advisor in scholar["advisors"]:
                # edges.append(
                #     {"advisor": trans[advisor], "student": trans[scholar["name"]]}
                # )
                edges.append(
                    {
                        "source": advisor,
                        "target": scholar["name"],
                    }
                )
        except KeyError:
            pass

    with open("public/data.json", "w") as f:
        json.dump({"nodes": nodes, "edges": edges}, f)
