"use strict";

const width = 0.99 * window.innerWidth;
const height = 0.99 * window.innerHeight;

const svg = d3.select("svg")
    .attr("width", width)
    .attr("height", height);

const g = svg.append("g");

svg.call(d3.zoom()
    .on("zoom", ({
        transform
    }) => {
        g.attr("transform", transform);
    }));

svg
    .append("defs")
    .append("marker")
    .attr("id", "arrow")
    .attr("refX", 17)
    .attr("refY", 6)
    .attr("markerWidth", 12)
    .attr("markerHeight", 12)
    .attr("orient", "auto")
    .append("path")
    .attr("d", "M 0 0 12 6 0 12 3 6")
    .attr("opacity", 0.6)
    .style("fill", "grey");

d3.json("data.json").then(data => {

    const simulation = d3.forceSimulation(data.nodes)
        .force("charge", d3.forceManyBody().strength(-5e2))
        .force("link", d3.forceLink(data.edges).id(d => d.id))
        .force("center", d3.forceCenter(width / 2, height / 2));

    const linkElements = g
        .append("g")
        .selectAll("line")
        .data(data.edges)
        .join("line")
        .attr("stroke", "grey")
        .attr("stroke-opacity", 0.6)
        .attr("marker-end", "url(#arrow)");

    const nodeElements = g
        .append("g")
        .selectAll("circle")
        .data(data.nodes)
        .join("circle")
        .attr("r", 5)
        .attr("fill", "grey")
        .call(d3.drag()
            .on("start", (event, d) => {
                if (!event.active) {
                    simulation.alphaTarget(0.3).restart();
                }
                d.fx = d.x;
                d.fy = d.y;
            })
            .on("drag", (event, d) => {
                d.fx = event.x;
                d.fy = event.y;
            })
            .on("end", (event, d) => {
                if (!event.active) {
                    simulation.alphaTarget(0);
                }
                d.fx = null;
                d.fy = null;
            }));

    // nodeElements.append("title")
    //     .text(d => d.id);

    const textElements = g
        .append("g")
        .selectAll("text")
        .data(data.nodes)
        .join("text")
        .text(node => node.id)
        .attr("font-size", 10)
        .attr("dx", d => 5.5 * (-d.id.length / 2))
        .attr("dy", 20)

    simulation
        // .nodes(data.nodes)
        .on("tick", () => {
            nodeElements
                .attr("cx", d => d.x)
                .attr("cy", d => d.y)
            textElements
                .attr("x", d => d.x)
                .attr("y", d => d.y)
            linkElements
                .attr("x1", d => d.source.x)
                .attr("y1", d => d.source.y)
                .attr("x2", d => d.target.x)
                .attr("y2", d => d.target.y);
        })

})
